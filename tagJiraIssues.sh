#!/bin/bash

# arguments
# $1 - label name, e.g. 'deployed-beta'
# $2 - whitespace separated list of JIRA project IDs to look for (e.g. "WM CLIN")
# $3 - authorized user email
# $4 - API token
# $5 - (optional) JIRA commit history length, # of commits

gitLogFile="git-log.txt"
commitCount=${5:-200}

echo Looking into latest $commitCount commits

git log -n $commitCount > $gitLogFile

for project in $2
do
  echo Tagging issues in $project ...
  grep $project- $gitLogFile | sed "s/^.*\($project-[[:digit:]]\+\).*$/\1/" | sort | uniq > uniq-$project.txt

  while read p; do
    echo tagging $p with $1 
    curl --request PUT \
      --url "https://fimmic.atlassian.net/rest/api/3/issue/$p" \
      --user "$3:$4" \
      --header 'Accept: application/json' \
      --header 'Content-Type: application/json' \
      --data "{ \"update\": { \"labels\": [ { \"add\": \"$1\" } ] } }"
  done <uniq-$project.txt
done