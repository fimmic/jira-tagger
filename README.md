# README #

This repository provides utilities for labeling JIRA issues, e.g. adding deployments

## Usage

Prerequisites:
* JIRA Project(s)  :)
* authorized user email
* API key

Running:
Note: You need to run the script inside a relevant git repo, in order to get reasonable commit history

command
```
$> ./tagJiraIssues.sh <tag> <JIRA projects> <email> <API key> [history length]
```
e.g. 
```
$> ./tagJiraIssues.sh deployed-beta "WM CLIN" super.user@aiforia.com 12345678qwerty 100
```
will fetch the latest 100 commits on current branch, parse issue IDs under projects `WM` and `CLIN`, and tag the issues found with `deployed-beta`