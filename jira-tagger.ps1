param($commitCount,
      [string[]]$projects=@("WM", "CLIN"),
      [parameter(mandatory)] $email,
      [parameter(mandatory)] $token,
      [parameter(mandatory)] $jiraLabel)


write-host "Looking into latest $commitCount commits"
write-host "Filtering for projects: $projects"

$gitLogFile="git-log.txt"
git log -n $commitCount | Out-File -FilePath $gitLogFile

$encodedCreds = [System.Convert]::ToBase64String([System.Text.Encoding]::ASCII.GetBytes("$($email):$($token)"))
$basicAuthValue = "Basic $encodedCreds"

$Headers = @{
    Authorization = $basicAuthValue
}

$updateBody = "{`"update`": { `"labels`": [ { `"add`": `"$jiraLabel`" } ] } }"

Foreach ($project in $projects)
{
    write-host "Labeling issues in $project"
    $uniqueIssues = Select-String -path $gitLogFile "($project-\d+)" |
        Sort-Object -InputObject {$_.Matches} |
        Get-Unique

    Foreach ($issue in $uniqueIssues)
    {
        try
        {
            Write-Host "Labeling $issue..."
            $uri = "https://aiforia.atlassian.net/rest/api/3/issue/$issue"
            [Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12
            Invoke-WebRequest -Uri $uri -Method Put -ContentType "application/json" -Headers $Headers -Body $updateBody
        }
        catch
        {
            # let's not fail a build because of this
            Write-Host $_.Exception
        }
    }
}
